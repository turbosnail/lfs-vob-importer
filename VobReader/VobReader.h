#ifndef VOBREADER_H
#define VOBREADER_H

#include "vob.h"

using namespace std;
#include <windows.h>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>

struct FileBuffer
{
    u_int   FilePosition;
    u_int   FizeSize;
    char*   Buffer;
};

class VobReader
{
    public:
        VobReader(string car);
        virtual ~VobReader();
        bool loadFile();
        bool parse();


    private:
        FileBuffer buffer;
        string fileName;
        string debugFileName;
        ofstream debugFile;
        SreObj* object;

        u_int readBlock(void* Dest, size_t sizeToRead);
        int ReadChildBlock();
};

#endif // VOBREADER_H
