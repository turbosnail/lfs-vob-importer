loadDefaultMatLib() 

global VobEditorDialog, globalObjectIterator = 1, lfsdir

global lfseditordir 

fn ReadBytes2String fs len =
(
	local str = ""
	local length = 0
	local finished = false
	for i = 1 to len do
	(
		local c = ReadByte fs #unsigned
		if c == 0 then finished = true
		if finished then continue	
		str += bit.intAsChar(c)			
		if c != 32 then length = i		
	)
	substring str 1 length
)

struct DDSFile
(
	flag,
	Name,
	theMap
)

struct Mat
(
	Name,
	ddsID
)

struct face
(
	ObjID,
	Smooth,
	Ind1,
	Ind2,
	Ind3
)

struct ObjectHeader
(
	NumVertexs,
	NumFaces,
	rX,
	rY,
	rZ,
	rot_dummy,
	X,
	Y,
	Z,
	NumObjects,
	fn ReadHeader fs = 
	(
		local NA3 = readshort fs
		local NA4 = readlong fs
		local NA5 = readlong fs
		local Info = readshort fs
		
		NumVertexs = readshort fs
		NumFaces = readshort fs
		
		readlong fs
		
		rX = (readshort fs)*360/65536
		rY = (readshort fs)*360/65536
		rZ = (readshort fs)*360/65536
		
		readshort fs
		
		rot_dummy = eulerangles rX rY rZ
		
		X = (readlong fs) *1.0 / 65536.0
		Y = (readlong fs) *1.0 / 65536.0
		Z = (readlong fs) *1.0 / 65536.0
		
		local One = readshort fs
		NumObjects = readshort fs
	)
)

struct SubObject
(
	R, 
	G, 
	B, 
	X, 
	Name, 
	MatID,
	fn Read fs = 
	(
		R = readbyte fs #unsigned
		G = readbyte fs #unsigned
		B = readbyte fs #unsigned
		X = readbyte fs #unsigned
		-- read materilname
		
		Name = ReadBytes2String fs 10				
				
		-- !read materilname
		MatID = readshort fs
		MatID += 1
	)
)

struct FaceStruct
(
	MirrorState,
	CarGroupID,
	ObjID,
	Type,
	Colision,
	SmoothG,
	Ind1,
	Ind2,
	Ind3,
	
	fn Read fs = 
	(
		MirrorState = readbyte fs #unsigned
		CarGroupID = readbyte fs #unsigned		
		ObjID = readbyte fs #unsigned -- ObjID
		
		Type = readbyte fs #unsigned
		Colision = readbyte fs #unsigned -- collision
		SmoothG = readbyte fs #unsigned -- smooth group

		Ind1 = readshort fs -- index 1
		Ind2 = readshort fs -- index 2
		Ind3 = readshort fs -- index 3
		
		if Ind1 < 0 then (Ind1 += 32768)
		if Ind2 < 0 then (Ind2 += 32768)
		if Ind3 < 0 then (Ind3 += 32768)
		
		SmoothG += 1
		ObjID += 1
		Ind1 += 1
		Ind2 += 1
		Ind3 += 1		
	)
)

struct TextureDescription
(
	Name,
    skyReflection,
    Rotate,
    TextureId,
    Sp1,
    UTile,
    VTile,
    UOffset,
    VOffset,
	
	fn Read fs = 
	(
		Name = ReadBytes2String fs 16
		skyReflection  = readbyte fs #unsigned
		Rotate = readbyte fs #unsigned
		TextureId = readbyte fs #unsigned
		Sp1 = readbyte fs #unsigned
		UTile = (readbyte fs #unsigned)*1.0/64.0
		VTile = (readbyte fs #unsigned)*1.0/64.0
		UOffset = (readbyte fs #unsigned)*1.0/64.0
		VOffset = (readbyte fs #unsigned)*1.0/64.0
	)
)

struct UVBox
(
	Name,
	Flag,

	RightBoundUVX,
	LeftBoundUVX,
	DownBoundUVY,
	UpperBoundUVY,
    -- temp values
	UTile,
    VTile,
    UOffset,
    VOffset,
	-- end temp values
	RTDI,
	LTDI,
	
	fn Read fs = 
	(
		Name = ReadBytes2String fs 16
		Flag = readlong fs

		RightBoundUVX = (readlong fs) *1.0 / 65536.0
		LeftBoundUVX = (readlong fs) *1.0 / 65536.0
		DownBoundUVY = (readlong fs) *1.0 / 65536.0
		UpperBoundUVY = (readlong fs) *1.0 / 65536.0

		RTDI = readlong fs #unsigned
		LTDI =readlong fs #unsigned
	)
)
------------------------------------- end of Object Header.ms



function ReadChild fs skipNum totalVobSize = 
(
	local main_ob = Dummy()	
	
	local ob_name = "Object_" + (globalObjectIterator as string)	
	main_ob.name =  ob_name
	main_ob.boxsize = [0.001, 0.001, 0.001]
	
	local objHeader = ObjectHeader()
	
	objHeader.ReadHeader fs	
	
	local objts = #()
	local verts_flag = #()
	local vertex_array = #()	
	local faces= #()	
	local Textures = #()
	local TexturesDescrptions = #()
	local UVBoxes = #()
	
	local minX=10, maxX=-10, minY=10, maxY=-10, minZ=10, maxZ=-10
	local length=0, width=0,  height=0

	
	
	for i=1 to objHeader.NumObjects do 
	(	
		local obj = SubObject()
		
		obj.Read fs
		
		append objts obj
	)
	
	

	for i=1 to objHeader.NumVertexs do 
	(
		local flag = readlong fs
		local x = (readlong fs) *1.0 / 65536.0
		local y = (readlong fs) *1.0 / 65536.0
		local z = (readlong fs) *1.0 / 65536.0
		
		-- calc min/max values
		if(x < minX) do
			minX = x
			
		if(x > maxX) do
			maxX = x
		
		if(y < minY) do
			minY = y
		
		if(y > maxY) do
			maxY = y
		
		if(z < minZ) do
			minZ = z
		
		if(z > maxZ) do
			maxZ = z
		
		
		-- end of calc min/max values	
		local v = [x, y, z]
		append vertex_array v
		append verts_flag flag
	)
	

	width = maxX - minX
	length = maxY - minY
	height = maxZ - minZ
	
	local t = #()
	
	for i=1 to objHeader.NumFaces do 
	(
				
		local face = FaceStruct()
		face.Read fs

		if(faces[face.ObjID] == undefined) then
		(
			faces[face.ObjID]  = #()
		)
		
		t[ face.Ind1 ] = true
		t[ face.Ind2 ] = true
		t[ face.Ind3 ] = true

		append faces[face.ObjID] face
	)
	print "find verts without faces"
	for i=1 to objHeader.NumVertexs do 
	(
		if(bit.and verts_flag[i] 64) then
		(
			local sphr = Sphere()
			sphr.parent = main_ob
			sphr.radius = 0.01
			sphr.pos = vertex_array[i]
			--print i 
			--print verts_flag[i]
		)
	)
		
-- THIS HACK FOR FX car
	local skip = 0
	while skip == 0 do
	(
		skip = readbyte fs
	)
	
	fseek fs -1 #seek_cur
	
	local ddsCount = readlong fs
	
	for i=1 to ddsCount do
	(
		local ss = StringStream""
		format "%\\data\\" lfsdir to:ss
		
		local flag = readlong fs
		
		if (flag == 1) then
		(
			format "skins_dds\\XFR_" to:ss
		)
		else
		(
			format "dds\\" to:ss
		)
		
		local Name = ReadBytes2String fs 16

		format "%.dds" Name to:ss
		
		Name = ss as string
		
		print Name
		
		local dd = DDSFile()
		dd.flag = flag 
		dd.Name = Name
		dd.theMap = openBitmap Name
		print dd.theMap
		append Textures dd		
	)

	local TextureDescriptionCount = readlong fs

	
	
	
	for i=1 to TextureDescriptionCount do
	(
		local td = TextureDescription()
		td.Read fs
		
		append TexturesDescrptions td
	)
		
	local MaterialCount = readlong fs
	
	local multimat = MultiMaterial()
	multimat.name = main_ob.name
	multimat.numsubs = MaterialCount
	
	setMeditMaterial globalObjectIterator multimat 
	
	for i=1 to MaterialCount do
	(
		local uv = UVBox()
		uv.Read fs
		
		multimat.names[i] =  uv.Name
		
		multimat[i].showInViewport = true
		multimat[i].name = uv.Name
		multimat[i].diffuse = color objts[i].R objts[i].G objts[i].B
		
		local ddsfile = Textures[TexturesDescrptions[uv.RTDI+1].TextureId+1]
		
		if(ddsfile.theMap != undefined) do
			multimat[i].diffuseMap = (bitMaptexture bitMap:ddsfile.theMap) 
		
		showTextureMap multimat[i] true
		
		uv.UTile = TexturesDescrptions[uv.RTDI+1].UTile
		uv.VTile = TexturesDescrptions[uv.RTDI+1].VTile
		uv.UOffset = TexturesDescrptions[uv.RTDI+1].UOffset
		uv.VOffset = TexturesDescrptions[uv.RTDI+1].VOffset
		
		append UVBoxes uv
		
	)
		
 	local pos = ftell fs

	if(pos < totalVobSize) do(
		fseek fs (8*skipNum) #seek_cur -- skeep 
	)

-- 	Lest create
	
	for i=1 to objHeader.NumObjects do
	(
		if (faces[i] != undefined and faces[i].count > 0) then 
		(
			
			local lVerts = #()
			local lFaces = #()
			local OldIndex2New=#(), arrayIndex=1
			
			for j = 1 to faces[i].count do
			(

				local v1 = vertex_array[ faces[i][j].Ind1 ] as point3
				local v2 = vertex_array[ faces[i][j].Ind2 ] as point3 
				local v3 = vertex_array[ faces[i][j].Ind3 ] as point3
					
				if(OldIndex2New[ faces[i][j].Ind1 ] == undefined) then
				(
					OldIndex2New[ faces[i][j].Ind1 ] =  arrayIndex
					arrayIndex = arrayIndex + 1
					append lVerts v1
				)
				
				if(OldIndex2New[ faces[i][j].Ind2 ] == undefined) then
				(
					OldIndex2New[ faces[i][j].Ind2 ] =  arrayIndex
					arrayIndex = arrayIndex + 1
					append lVerts v2
				)
				
				if(OldIndex2New[ faces[i][j].Ind3 ] == undefined) then
				(
					OldIndex2New[ faces[i][j].Ind3 ] =  arrayIndex
					arrayIndex = arrayIndex + 1
					append lVerts v3
				)
				
				local f = [OldIndex2New[faces[i][j].Ind1], OldIndex2New[faces[i][j].Ind2], OldIndex2New[faces[i][j].Ind3]]
				
				append lFaces f
			)
			
			local ob = mesh vertices:lVerts faces:lFaces tverts:lVerts
			ob.parent = main_ob
			ob.name = objts[i].Name
			ob.material = multimat[objts[i].MatID]

					

			buildTVFaces ob
			
			for f = 1 to ob.numfaces do
					setTVFace ob f (getface ob f)	
			
			for j = 1 to faces[i].count do
			(
				setFaceMatID ob j objts[i].MatID
				setFaceSmoothGroup ob j faces[i][j].SmoothG			
			)
			
			ConvertTo ob Editable_Poly;			
			
			uvm = UVWMap()
			uvm.name = UVBoxes[objts[i].MatID].Name
			uvm.utile = UVBoxes[objts[i].MatID].UTile
			uvm.vtile = UVBoxes[objts[i].MatID].VTile
			uvm.length = length
			uvm.width = width
			uvm.height = height
			addModifier ob uvm
		)
	) 
	
	
-- 	After create all meshs move and rotate dummy
	main_ob.pos.x = objHeader.X
	main_ob.pos.y = objHeader.Y
	main_ob.pos.z = objHeader.Z
	
	rotate main_ob objHeader.rot_dummy	

	globalObjectIterator += 1
)

----------------------------------------------------------
-- export section
----------------------------------------------------------

function fillZero fs count =
(
	for i = 1 to count do
	(
		writebyte fs 0 #unsigned	
	)
)

function WriteStringSize fs text size = 
(
	local len=text.count
	
	writestring fs text
	
	for i=text.count to (size-2) do
	(
		writebyte fs 0
	)
	
)

struct MAP
(
	Name,
	Color,
	Heading,
	Roll,
	Pitch,
	Width,
	Height,
	Len,
	
	Right,
	Front,
	Up,
	
	--tmp
	minX=1024,
	maxX=-1024,
	minY=1024,
	maxY=-1024,
	minZ=1024,
	maxZ=-1024
)

function exportSRE filename =
(
	local numLODs =1, numVertex=0, numFaces=0, numPages=2, numCutouts=2, numMaps=2
	
	print filename
	
	local fs = fopen filename "wb"
	-- header
	writestring fs "SREOBJ"
	writebyte fs 252 #unsigned
		
	for curobj in objects	 do
	(
		local vertexes = #()
		local TVvertexes = #()
		local faces = #()
		local maps = #()
		local cutouts = #()
		local pages= #()
		
		
		for i=1 to 24 do
		(
			local m = MAP()
			append maps m
		)
	
		print curobj.name
		convertToMesh curobj
		
		print (getNumVerts curobj)
		print (getNumTVerts curobj)

		
		writebyte fs 13 #unsigned -- always 13
		writebyte fs 0 #unsigned -- type 0 - main
		writebyte fs 1 #unsigned -- flip
		writebyte fs numLODs #unsigned -- LOD count
		
		fillZero fs 20
		
		writebyte fs 64 #unsigned -- subObjState
		writebyte fs 0 #unsigned
		writebyte fs 3 #unsigned
		writebyte fs 0 #unsigned
		
		--lod info
		for i=1 to numLODs do
		(
			writeshort fs 0 #unsigned
			writeshort fs 120 #unsigned
		)
		--geometry info
		
		writeshort fs (getNumVerts curobj) #unsigned -- NumVertex
		writeshort fs (getNumFaces curobj) #unsigned  -- NumVertex
		
		WriteLong fs 0 --some count
		
		--rotation
		writeshort fs (0*32768) #unsigned --x
		writeshort fs (0*32768) #unsigned --y
		writeshort fs (0*32768) #unsigned --z
		
		writeshort fs 0 #unsigned --zero spare
		
		-- position
		WriteLong fs (0*65536) --x
		WriteLong fs (0*65536) --y
		WriteLong fs (0*65536) --z
		
		--vertex buffer
		
		for i=1 to (getNumVerts curobj) do
		(
			vert = getVert curobj i
			
			append vertexes vert
			
			WriteLong fs 0         --flag
			WriteLong fs (vert.x*65536) --x
			WriteLong fs (vert.y*65536) --y
			WriteLong fs (vert.z*65536) --z
		)
		
		--faces buffer
		
		for i=1 to (getNumFaces curobj) do
		(
			face= getFace curobj i
			TVface = getTVFace curobj i
			matID = getFaceMatID curobj i
			
			
			for j=1 to 3 do
			(
				v = getVert curobj face[j]
				-- X
				if (maps[matID].minX > v.x) then
				(
					maps[matID].minX = v.x
				)
				
				if (maps[matID].maxX < v.x) then
				(
					maps[matID].maxX = v.x
				)
				-- Y
				if (maps[matID].minY > v.y) then
				(
					maps[matID].minY = v.y
				)
				
				if (maps[matID].maxY < v.y) then
				(
					maps[matID].maxY = v.y
				)
				-- Z
				if (maps[matID].minZ > v.z) then
				(
					maps[matID].minZ = v.z
				)
				
				if (maps[matID].maxZ < v.z) then
				(
					maps[matID].maxZ = v.z
				)
			)
			

			
			append faces face
			
			writebyte fs 0
			writebyte fs 0
			writebyte fs (matID-1) -- material
			writebyte fs 0
			writebyte fs 0
			writebyte fs (getFaceSmoothGroup curobj i)
			
			writeshort fs (face[1]-1) #unsigned --index 1
			writeshort fs (face[2]-1) #unsigned --index 2
			writeshort fs (face[3]-1) #unsigned --index 3
		)
		
		for i=1 to 24 do
		(
		
			material = getMeditMaterial i
			materialType = (classOf material) as string
			
			if (materialType == "FLS_Mtl") then (
				maps[i].Color = material.color
			) else (
				maps[i].Color = material.diffuse_color
			)
			
			maps[i].Name = material.name
			
			print materialType
			
			maps[i].Width = maps[i].maxX - maps[i].minX
			maps[i].Len = maps[i].maxY - maps[i].minY
			maps[i].Height = maps[i].maxZ - maps[i].minZ
			 
			maps[i].Right = maps[i].maxX  - maps[i].Width/2
			maps[i].Front = maps[i].maxY  - maps[i].Len/2
			maps[i].Up = maps[i].maxZ  - maps[i].Height/2

		)
		
		--pages
		
		WriteLong fs numPages
		
		for i=1 to numPages do
		(
			if (i==1) then (
			WriteLong fs 0
			WriteStringSize fs "Cromowheel2" 20
			) else (
			WriteLong fs 1
			WriteStringSize fs "DEFAULT" 20
			)
		)
		
		--cutouts
		
		WriteLong fs numCutouts
		
		for i=1 to numCutouts do
		(
			WriteStringSize fs "unnamed" 16
			--rotate
			writebyte fs 0
			--flag
			writebyte fs 0
			-- page id
			writeshort fs (i-1)
			--AlphaType
			writebyte fs 0
			--EnvMapType
			writebyte fs 0
			--ShineType
			writebyte fs 0
			--spare
			writebyte fs 0
			--color
			writebyte fs 255
			writebyte fs 255
			writebyte fs 255
			--roughness
			writebyte fs 0
			--size
			writebyte fs 128
			writebyte fs 128
			--pos
			writebyte fs 0
			writebyte fs 0
		)
		
		--maps
		
		writeshort fs numMaps
		writeshort fs 2 --always
		
		for i=1 to numMaps do
		(
			WriteStringSize fs maps[i].Name 16

			--color
			writebyte fs maps[i].Color.R
			writebyte fs maps[i].Color.G
			writebyte fs maps[i].Color.B
			
			--spare
			writebyte fs 0
			
			--flag
			writelong fs 0
			--rotate
			writelong fs 1359872
			
			--locate
			writelong fs (maps[i].Right*65536) --X (right)
			writelong fs (maps[i].Front*65536) --Z (front)
			writelong fs (maps[i].Up*65536) --Y (up)
			
			--width
			writelong fs (maps[i].Width*65536)
			--height
			writelong fs (maps[i].Len*65536)
			
			--cutout index
			writeshort fs (i-1)
			-- oposite cutout index
			writeshort fs (i-1)
		)
		
		for i=1 to 16 do
		(
			WriteStringSize fs "test" 8
		)
		
		fillZero fs 128
	)
	fclose fs
)
-------------------------------------------------------------- end of tools.ms 



try(destroydialog VobEditorDialog)catch()
rollout VobEditorDialog "Live For Speed Vob Editor" 
(
    local vobFileName = ""
	local totalVobSize = 0
	
	button VobFileButton "Select Vob File"
	button LFSRootDirButton "Select LFS root"
	button ImportButton "Import VOB"
	
	
	on VobFileButton pressed do 
	(
		vobFileName = getOpenFilename \
		caption:"Select *.vob File" \
		types: "LFS Car Model(*.vob)|*.vob|All files (*.*)|*.*|"
		
		--carName = substring vobFileName (vobFileName.count - 5) 2
		
		--print carName
	)
	
	on LFSRootDirButton pressed do 
	(
		lfsdir = getSavePath caption:"Select LFS root dir" initialDir:"D:\games\games\LFS"
	)
	
	on ImportButton pressed do 
	(
		globalObjectIterator = 1
		resetMaxFile()
		clearlistener()
		if vobFileName != undefined then
		(
			
			if lfsdir != undefined then
			(
				print vobFileName
				print lfsdir
				
				totalVobSize = GetFileSize vobFileName
				
				local fs = fopen vobFileName "rb"
				
				if fs != undefined then
				(
					-- start body 
					fseek fs 32 #seek_set

					ReadChild fs 34 totalVobSize
					local pos = 1
					do(
						ReadChild fs 2 totalVobSize
						pos = ftell fs
					)
					while pos < totalVobSize
					fclose fs
				)
				else
				(
					print "Can't open file in binary mode"
				)
			)
			else
			(
				print "LFS root dir is undefined"
			)
		
		)
		else
		(
			print "Vob File Not Selected"
		)
		
	)
	
	-- export section
	
	local sreFileName
	
	button LFSEditorPath "LFS Editor Path"
	button ExportButton "Export SRE"
	
	on LFSEditorPath pressed do
	(
		lfseditordir = getSavePath caption:"Select LFS Editor root dir" initialDir:"D:\games\games\LFS_EDITOR"
	)
	
	on ExportButton pressed do
	(
		sreFileName = getSaveFilename \
		caption:"Select *.sre File" \
		types: "LFS Car Model(*.sre)|*.sre|All files (*.*)|*.*|"
		
		exportSRE sreFileName
	)
)
createDialog VobEditorDialog 200 200