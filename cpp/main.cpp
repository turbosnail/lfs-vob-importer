/**
*       This programm read Live For Speed S3 .VOB files
*
**/

#include<list>
#include "VobReader/VobReader.h"

int main()
{
    list<string>cars  = {/*"F1", "F8", "FB", "FO", "FX", "FZ", "LX", "MR", "RA", "RB",*/ "UF"/*, "XF", "XR"*/};

    for(auto car:cars) {
        VobReader* reader = new VobReader(car);
        if(reader->loadFile()) {
            reader->parse();
        }
        delete reader;
    }

    system("Pause");
    return 0;
}
