#ifndef VOB_H_INCLUDED
#define VOB_H_INCLUDED

typedef unsigned char byte;
typedef signed short word;
typedef unsigned short u_word;
typedef unsigned int u_int;

typedef struct
{
    int X;
    int Y;
    int Z;
} Vector3;

typedef struct
{
    word X; // 32768 - 180 g
    word Y;
    word Z;
} Vec3R;


struct Header // 32 bytes
{
    char    FileType[7];    //7
    byte    Na;             //1 F9 or FA or FB else crash game
};

struct Child
{
    char Z[16];
    u_word Key;
    u_word Key2;
    byte Some1Count;
    byte unk;
    u_word NaCount;
};

struct Some{
    u_word Zero;
    u_word SomeRotate;
};

struct GeometryInfo{
    word    NumVertex;      //2
    word    NumFaces;       //2
    u_int    CountNotice;
    Vec3R   rot;
    word    Zero;
    Vector3 pos;            // position
    word    One;            //2
    word    NumObjects;    //2
};

struct ObjectName // 16 byte
{
    byte R; // Red
    byte G; // Green
    byte B; // Blue
    byte X; // N/A
    char Name[10];
    word MaterialID;
};

struct Vertex // size = 16 byte
{
    int NA;
    int X; // float = 4 byte
    int Y;
    int Z;
};

struct Face // 12 bytes
{
    byte MirrorState;   // SEE BELOW
    byte CarGroupID;    // Car Object Group
    byte ObjectNameID;         // Id in ObjectName array
    byte Type;          // 0 1 2 3
    byte Colision;      // 0 1 2 (2 - collision)
    byte Smooth;        // �����������
    u_word Ind1;
    u_word Ind2;
    u_word Ind3;
};

enum MIRROR_STATE
{
    MIRROR_OFF = 0,
    MIRROR_ON = 1,
    OLOLO_2 = 2,
    GLASS = 4,
    OLOLO_8 = 8,
    MIRROR_FIX = 16
};

struct DDSFile // 20 byte
{
    int  Flag; // 1 - locate in /skins_dds/CAR_DEFAULT.dds
    char Name[16];
};

struct TextureDescription // 24 byte
{
    char    Name[16]; //16
    byte    skyReflection;      // bit 0 : sky reflection
    byte    Rotate;             //=0 1=90 2=180 3=270
    byte    TextureId;          // world texture index
    byte    Sp1;                // see NOTES (5)
    byte    UTile;             //
    byte    VTile;             // 64 = 1.0
    byte    Uoffset;              // 64 = 1.0
    byte    Voffset;              // 64 = 1.0

};

struct UVWMatrix // 44 byte
{
    char Name[16]; //16
    int Flag;      //4
    int X;     //4
    int Y;     //4
    int Width;     //4
    int Height;     //4
    int RTDI;     //    RightSide Texture Descriptions ID
    int LTDI;     //    LeftSide  Texture Descriptions ID
};

struct SubObject
{
    Child child;
    Some* some;
    GeometryInfo geometryInfo;
    ObjectName*  objectNames;
    Vertex*      vertexes;
    Face*        faces;

    int DDSFileCount;
    DDSFile* DDSFiles;

    int TextureDescriptionCount;
    TextureDescription TextureDescriptions;

    int MaterialCount;
    UVWMatrix* UVWMatrixs;
};

struct SreObj
{
    Header header;
    SubObject* subObject;
};

#endif // VOB_H_INCLUDED
