#include "VobReader.h"

VobReader::VobReader(string fileName)
{
    //ctor
    this->fileName =  fileName + ".vob";
    this->debugFileName = fileName + ".txt";
    memset(&this->buffer,0,sizeof(FileBuffer));
}

VobReader::~VobReader()
{
    //dtor
    this->debugFile.close();
    free(&this->buffer);
}

bool
VobReader::loadFile()
{
    memset(&buffer,0,sizeof(FileBuffer));

    this->debugFile.open(this->debugFileName,ios::out);

    HANDLE fff;
    WIN32_FIND_DATA fd;
    fff = FindFirstFile(this->fileName.c_str(),&fd);
    if (fff == INVALID_HANDLE_VALUE)
    {
        cout << "Can't find " << this->fileName << endl;
        system("Pause");
        return -1;
    }
    FindClose(fff);

    HANDLE  hFile;

    // ��������� ���� ��� ������
    hFile = CreateFile(
                this->fileName.c_str(),     // ��� �����
                GENERIC_READ,            // ������ �� �����
                0,                       // ����������� ������ � �����
                NULL,                    // ������ ���
                OPEN_EXISTING,           // ��������� ������������ ����
                FILE_ATTRIBUTE_NORMAL,   // ������� ����
                NULL        // ������� ���
            );
    // ��������� �� �������� ��������
    if (hFile == INVALID_HANDLE_VALUE)
    {
        printf("Create file failed. \nThe last error code: %d\nPress any key to finish.",(int)GetLastError());
        system("Pause");
        exit(1);
    }

    DWORD lpFileSizeL = 0;
    LPDWORD lpFileSizeH = 0;

    lpFileSizeL = GetFileSize(hFile, lpFileSizeH);

    buffer.FizeSize = lpFileSizeL;
    buffer.Buffer = (char*) calloc (lpFileSizeL,sizeof(char));

    if (buffer.Buffer == NULL)
        exit(1);


    DWORD dwBytesRead = 0;

    if ( !ReadFile(hFile,buffer.Buffer,lpFileSizeL,&dwBytesRead,(LPOVERLAPPED)NULL) )
    {
        CloseHandle(hFile);
        cerr << "Read file failed." << endl;
        system("Pause");
        exit(1);
    }

    CloseHandle(hFile);

    return true;
}

bool
VobReader::parse()
{
    this->object = new SreObj();

    this->readBlock(&this->object->header,sizeof(Header));

    do{
        this->ReadChildBlock();
    }
    while(this->buffer.FilePosition < this->buffer.FizeSize);
// 170844 wheel C2
// 190168 break
// 191016 break 2
// 191784 GT GT+T gear shift
// 194588 seat
// 197856 mirror
// 201260 windscreen wiper GT
// 203924 windscreen wiper GT+T
// 206588 windscreen wiper GTR
// 209252 seat GTR
// 217872 hood lock L
// 219928 hood lock R
// 221984 dummy 1
// 222160 dummy 2
// 222336 dummy 3
// 222512 door knob
// 224376 GTR panel switches
// 236524 steering shell
// 240708 NOS
// 250140 GTR gear shift
// 259904 GTR speedometer
// 265328 GTR speedometer switchers
// 275952 belt attachment
    // EOF
    return true;
}

u_int
VobReader::readBlock(void* Dest, size_t sizeToRead)
{
    // ���� �� ����� ������� �� ������ ������� ������� ��� ����, ������ ���
    if(buffer.FilePosition+sizeToRead > buffer.FizeSize)
    {
        cout << "You try read more than have" << endl;
        cout << "File Position " << buffer.FilePosition << endl;
        cout << "Try to read " << sizeToRead << endl;
        return 0;
    }

    memcpy(Dest,buffer.Buffer+buffer.FilePosition,sizeToRead);
    buffer.FilePosition += sizeToRead;

    return sizeToRead;
}

int
VobReader::ReadChildBlock()
{
    struct Child  Chld;
    readBlock(&Chld,sizeof(Child));

    struct Some s[Chld.Some1Count];
    readBlock(&s,sizeof(Some)*Chld.Some1Count);

    struct GeometryInfo Geometry;
    readBlock(&Geometry,sizeof(GeometryInfo));

    char msg[128];
    this->debugFile << "Dummy Rotation:" << endl;
    this->debugFile << "X: " << Geometry.rot.X*360/65536 << endl;
    this->debugFile << "Y: " << Geometry.rot.Y*360/65536 << endl;
    this->debugFile << "Z: " << Geometry.rot.Z*360/65536 << endl << endl;;

    this->debugFile << "Dummy Position:" << endl;
    this->debugFile << "X: " << Geometry.pos.X << endl;
    this->debugFile << "Y: " << Geometry.pos.Y << endl;
    this->debugFile << "Z: " << Geometry.pos.Z << endl << endl;;

    struct ObjectName ObjectNames[Geometry.NumObjects];
    struct Vertex vertexes[Geometry.NumVertex];
    struct Face faces[Geometry.NumFaces];


    this->debugFile << "\nSubObjects Info \n" ;
    sprintf(msg,"NumObjects = %d\n\n",Geometry.NumObjects);
    this->debugFile << msg ;
    readBlock(&ObjectNames,sizeof(ObjectName)*Geometry.NumObjects);
    int maxID = 0;
    for (int j = 0; j < Geometry.NumObjects; j++)
    {


        char msg[128];
        sprintf (msg,"Name = %s\t\tMaterialID = %d\n",ObjectNames[j].Name, ObjectNames[j].MaterialID);
        this->debugFile << msg ;

        if(ObjectNames[j].MaterialID > maxID)
            maxID = ObjectNames[j].MaterialID;
    }

    this->debugFile << "\nMax MaterialID = " << maxID << " \n" ;

    this->debugFile << "\nVertex Info \n" ;
    sprintf(msg,"NumVertex = %d\n\n",Geometry.NumVertex);
    this->debugFile << msg ;
    readBlock(&vertexes,sizeof(Vertex)*Geometry.NumVertex);

    maxID = 0;
    int minX = 6553600; // 100 meters
    int maxX = 0;
    int minY = 6553600;
    int maxY = 0;
    int minZ = 6553600;
    int maxZ = 0;
    for(int j = 0; j< Geometry.NumVertex; j++) {

        if(vertexes[j].NA > maxID)
            maxID = vertexes[j].NA;

        if(vertexes[j].X < minX)
            minX = vertexes[j].X;
        if(vertexes[j].X > maxX)
            maxX = vertexes[j].X;

        if(vertexes[j].Y < minY)
            minY = vertexes[j].Y;
        if(vertexes[j].Y > maxY)
            maxY = vertexes[j].Y;

        if(vertexes[j].Z < minZ)
            minZ = vertexes[j].Z;
        if(vertexes[j].Z > maxZ)
            maxZ = vertexes[j].Z;

         if(vertexes[j].NA == 194)
            this->debugFile << j << " " ;
    }

    this->debugFile << "\nMax Vertex.NA = " << maxID << " \n\n" ;

    this->debugFile << "Min Vertex.X = " << minX << " (" << (float)minX / 65536 << " m)" << "\n" ;
    this->debugFile << "Max Vertex.X = " << maxX << " (" << (float)maxX / 65536 << " m)" << "\n" ;
    this->debugFile << "Min Vertex.Y = " << minY << " (" << (float)minY / 65536 << " m)" << "\n" ;
    this->debugFile << "Max Vertex.Y = " << maxY << " (" << (float)maxY / 65536 << " m)" << "\n" ;
    this->debugFile << "Min Vertex.Z = " << minZ << " (" << (float)minZ / 65536 << " m)" << "\n" ;
    this->debugFile << "Max Vertex.Z = " << maxZ << " (" << (float)maxZ / 65536 << " m)" << "\n" ;


    this->debugFile << "X length = " << ((float)(maxX - minX) / 65536) << " m" << "\n" ;
    this->debugFile << "Y length = " << ((float)(maxY - minY) / 65536) << " m" << "\n" ;
    this->debugFile << "Z length = " << ((float)(maxZ - minZ) / 65536) << " m" << "\n" ;

    this->debugFile << "\nFaces Info \n" ;
    sprintf(msg,"NumFaces = %d \n\n",Geometry.NumFaces);
    this->debugFile << msg ;
    readBlock(&faces,sizeof(Face)*Geometry.NumFaces);

    u_word maxVertexIndex = 0;
    map<u_word, bool> unused;

    map<byte, byte> CarGroupID;
    map<byte, byte> Smooth;
    map<byte, byte> Type;
    map<byte, byte> Colision;

    for (int i=0; i<Geometry.NumFaces; ++i) {

        if(faces[i].Ind1 > maxVertexIndex)
            maxVertexIndex = faces[i].Ind1;

        if(faces[i].Ind2 > maxVertexIndex)
            maxVertexIndex = faces[i].Ind2;

        if(faces[i].Ind3 > maxVertexIndex)
            maxVertexIndex = faces[i].Ind3;

        CarGroupID[faces[i].CarGroupID] = faces[i].CarGroupID;
        Smooth[faces[i].Smooth] = faces[i].Smooth;
        Type[faces[i].Type] = faces[i].Type;
        Colision[faces[i].Colision] = faces[i].Colision;

        unused[faces[i].Ind1] = true;
        unused[faces[i].Ind2] = true;
        unused[faces[i].Ind3] = true;
    }

    for( auto v:CarGroupID){
        this->debugFile << "CarGroupID = " << (int)v.first << endl ;
    }

    for( auto v:Smooth){
        this->debugFile << "Smooth = " << (int)v.first << endl ;
    }

    for( auto v:Type){
        this->debugFile << "Type = " << (int)v.first << endl ;
    }

     for( auto v:Colision){
        this->debugFile << "Colision = " << (int)v.first << endl ;
    }

    this->debugFile << "\nMax Vertex Index = " << maxVertexIndex%32768 << endl ;

    vector<u_word> tVerts;

    for(int j = 0; j< Geometry.NumVertex; j++) {

        if(unused[j] == false)
            tVerts.push_back(j);
    }

    this->debugFile << "\ntVerts = " << tVerts.size() << endl ;

    if ((Chld.Key & 192) != 192) {
        int u[Chld.NaCount];
        readBlock(&u,sizeof(int)*Chld.NaCount);

    }

    /** READ TEXTURES FILES **/

    int DDSFileCount = 0;

    readBlock(&DDSFileCount,sizeof(DDSFileCount));

    this->debugFile << "DDSFileCount = " << DDSFileCount << endl;
    this->debugFile << endl;
    struct DDSFile DDSFiles[DDSFileCount];
    /***/
    readBlock(&DDSFiles,sizeof(DDSFile)*DDSFileCount);
    /***/
    for (int j = 0; j < DDSFileCount; j++)
    {
        char msg[128];
        sprintf (msg,"DDSFiles[%d] {\n   Flag=%d\n TName=\"%s\"\n}\n\n",j,DDSFiles[j].Flag,DDSFiles[j].Name);
        //this->debugFile << msg ;
    }
    this->debugFile << endl;

    /** READ MAP DESCRIPTIONS **/

    int TextureDescriptionCount = 0;

    readBlock(&TextureDescriptionCount,sizeof(TextureDescriptionCount));

    this->debugFile << "TextureDescriptionCount = " << TextureDescriptionCount << endl;
    this->debugFile << endl;
    struct TextureDescription TextureDescriptions[TextureDescriptionCount];
    /***/
    readBlock(&TextureDescriptions, sizeof(TextureDescription)*TextureDescriptionCount);
    /***/
    char msg2[2048];
    sprintf (msg2,"id\tName\t\t\tskyReflection\tRotate\tTexId\tSp1\tUScale\tVScale\tUTile\tVTile\n");
    this->debugFile << msg2 ;
    for (int j = 0; j < TextureDescriptionCount; j++)
    {
        memset(&msg2,4,2048);

        sprintf(msg2, "%d\t%s\t\t\t%d\t%d\t%d\t%d\t%f\t%f\t%f\t%f\n",
                j,
                TextureDescriptions[j].Name,
                TextureDescriptions[j].skyReflection,
                TextureDescriptions[j].Rotate,
                TextureDescriptions[j].TextureId,
                TextureDescriptions[j].Sp1,
                (float)TextureDescriptions[j].UTile/64,
                (float)TextureDescriptions[j].VTile/64,
                (float)TextureDescriptions[j].Uoffset/64,
                (float)TextureDescriptions[j].Voffset/64
               );
        if(TextureDescriptions[j].TextureId == 0)
        this->debugFile << msg2 ;
    }

    this->debugFile << endl;

    /** READ INFO FOR MAPPING **/

    int UVWMatrixCount = 0;

    readBlock(&UVWMatrixCount,sizeof(UVWMatrixCount));

    this->debugFile << "UVWMatrixCount = " << UVWMatrixCount << endl;
    this->debugFile << endl;
    struct UVWMatrix UVWMatrixs[UVWMatrixCount];

    /***/
    readBlock(&UVWMatrixs,sizeof(UVWMatrix)*UVWMatrixCount);

    for (int j = 0; j < UVWMatrixCount; j++)
    {
        char msg[2048];
        sprintf (msg,"UVWMatrixs[%d] {\n   Name=\"%s\"\n   Flag=%d\n   X=%f\n   Y=%f\n   Width=%f\n   Height=%f\n   RTDI=%d\n   LTDI=%d\n}\n\n",
                 j,
                 UVWMatrixs[j].Name,
                 UVWMatrixs[j].Flag,
                 (float)UVWMatrixs[j].X / 65536,
                 (float)UVWMatrixs[j].Y / 65536,
                 (float)UVWMatrixs[j].Width / 65536,
                 (float)UVWMatrixs[j].Height / 65536,
                 UVWMatrixs[j].RTDI,
                 UVWMatrixs[j].LTDI
                );
        this->debugFile << msg;
    }

    if (Geometry.CountNotice > 0) {

        char FaceGroup[32][8];
        readBlock(&FaceGroup,sizeof(char)*8*32);

        for(int i = 0; i< 32; ++i)
        {
            char msg[2048];
            sprintf (msg,"Note: %s\n", FaceGroup[i]);
            this->debugFile << msg ;
        }
    }

    return 0;
}
